import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;

public class LogInGUI {

    private BufferedReader reader;
    private PrintWriter writer;

    private  JFrame logInFrame;

    private JTextField loginField;
    private JPasswordField passwordField;

    public LogInGUI(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void createLogInGUI(){

        logInFrame = new JFrame("LogIn");
        JPanel mainPanel = new JPanel();
        JPanel panelFieldLogin = new JPanel();
        JPanel panelFieldPassword = new JPanel();
        JPanel panelButtons = new JPanel();

        JLabel loginLabel = new JLabel("Login:");
        loginField = new JTextField(20);

        panelFieldLogin.setLayout(new BoxLayout(panelFieldLogin,BoxLayout.Y_AXIS));
        panelFieldLogin.add(loginLabel);
        panelFieldLogin.add(loginField);

        JLabel passwordLabel = new JLabel("Password:");
        passwordField = new JPasswordField(20);

        panelFieldPassword.setLayout(new BoxLayout(panelFieldPassword, BoxLayout.Y_AXIS));
        panelFieldPassword.add(passwordLabel);
        panelFieldPassword.add(passwordField);

        JButton logIn = new JButton("LogIn");
        logIn.addActionListener(new LogInButton());

        JButton sign = new JButton("Sign");
        sign.addActionListener(new SignButton());

        JButton recovery = new JButton("Recovery");
        recovery.addActionListener(new RecoveryListner());

        panelButtons.setLayout(new BoxLayout(panelButtons,BoxLayout.X_AXIS));
        panelButtons.add(logIn);
        panelButtons.add(sign);
        panelButtons.add(recovery);

        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        mainPanel.add(panelFieldLogin);
        mainPanel.add(panelFieldPassword);
        mainPanel.add(panelButtons);

        logInFrame.getContentPane().add(BorderLayout.NORTH, mainPanel);
        logInFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        logInFrame.setSize(270,200);
        logInFrame.setVisible(true);
    }

    class LogInButton implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            writer.println(loginField.getText() + ":" + passwordField.getText());
            writer.flush();

            if (Client.recvMessage(reader).equals("Server: You can send message!")) {
                new ChatGUI(reader,writer).createChatInterface();
                logInFrame.setVisible(false);
            }
            else{
                loginField.setText("");
                passwordField.setText("");
                loginField.requestFocus();
            }
        }
    }

    class SignButton implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            new RegisterGUI(reader,writer).createSignGUI();
            logInFrame.setVisible(false);

        }
    }

    class RecoveryListner implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            new RecoveryGUI(reader,writer).createRecoveryGUI();
            logInFrame.setVisible(false);
        }
    }
}
