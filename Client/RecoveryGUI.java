import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;

public class RecoveryGUI {

    private BufferedReader reader;
    private PrintWriter writer;

    private JFrame recoveryFrame;

    private JTextField emailField;

    public RecoveryGUI(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void createRecoveryGUI(){

        recoveryFrame = new JFrame("Recovery");
        JPanel mainPanel = new JPanel();
        JPanel panelFieldEmail = new JPanel();
        JPanel panelButtons = new JPanel();

        JLabel emailLabel = new JLabel("Email");
        emailField = new JTextField(20);

        panelFieldEmail.setLayout(new BoxLayout(panelFieldEmail, BoxLayout.Y_AXIS));
        panelFieldEmail.add(emailLabel);
        panelFieldEmail.add(emailField);

        JButton submit = new JButton("Submit");
        submit.addActionListener(new SubmitButton());

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new CancelButton());

        panelButtons.setLayout(new BoxLayout(panelButtons,BoxLayout.X_AXIS));
        panelButtons.add(submit);
        panelButtons.add(cancel);

        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));

        mainPanel.add(panelFieldEmail);
        mainPanel.add(panelButtons);

        recoveryFrame.getContentPane().add(BorderLayout.NORTH, mainPanel);
        recoveryFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        recoveryFrame.setSize(270,250);
        recoveryFrame.setVisible(true);
    }

    class SubmitButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            writer.println(emailField.getText());
            writer.flush();

            if (Client.recvMessage(reader).equals("Wrong email!")) {
                JOptionPane.showMessageDialog(null, "Wrong email!");
                emailField.setText("");
                emailField.requestFocus();
            }
            else{
                JOptionPane.showMessageDialog(null, "Check your email!");
                recoveryFrame.setVisible(false);
                new LogInGUI(reader,writer).createLogInGUI();
            }
        }
    }

    class CancelButton implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            new LogInGUI(reader,writer).createLogInGUI();
            recoveryFrame.setVisible(false);
        }
    }
}
