import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;

public class ChatGUI {

    private BufferedReader reader;
    private PrintWriter writer;

    private JTextArea incoming;
    private JTextField outgoing;



    public ChatGUI(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void createChatInterface(){

        JFrame frame = new JFrame("JavaChat");
        JPanel mainPanel = new JPanel();
        incoming = new JTextArea(15,50);
        incoming.setLineWrap(true);
        incoming.setWrapStyleWord(true);
        incoming.setEditable(false);
        JScrollPane qScroller = new JScrollPane(incoming);
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        outgoing = new JTextField(20);

        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(new SendButtonListner());
        Thread serverReader = new Thread(new IncomingReader(), "ServerReader");
        serverReader.start();

        mainPanel.add(qScroller);
        mainPanel.add(outgoing);
        mainPanel.add(sendButton);

        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,350);
        frame.setVisible(true);
    }

    public class SendButtonListner implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            writer.println(outgoing.getText());
            writer.flush();

            outgoing.setText("");
            outgoing.requestFocus();
        }
    }

    public class IncomingReader implements Runnable{

        public void run() {

            String message;

            try {
                while ((message = reader.readLine())!= null ) {
                    System.out.println(message);
                    incoming.append(message + "\n");
                }
            } catch (Exception e) {
                System.out.println("Connection with server is lost");
            }

        }
    }
}
