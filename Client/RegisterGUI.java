import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;

public class RegisterGUI {


        private BufferedReader reader;
        private PrintWriter writer;

        private JFrame signFrame;

        private JTextField loginField;
        private JPasswordField passwordField;
        private JTextField emailField;

        public RegisterGUI(BufferedReader reader, PrintWriter writer) {
            this.reader = reader;
            this.writer = writer;
        }

        public void createSignGUI(){

            signFrame = new JFrame("Sign");
            JPanel mainPanel = new JPanel();
            JPanel panelFieldLogin = new JPanel();
            JPanel panelFieldPassword = new JPanel();
            JPanel panelFieldEmail = new JPanel();
            JPanel panelButtons = new JPanel();

            JLabel loginLabel = new JLabel("Login:");
            loginField = new JTextField(20);

            panelFieldLogin.setLayout(new BoxLayout(panelFieldLogin,BoxLayout.Y_AXIS));
            panelFieldLogin.add(loginLabel);
            panelFieldLogin.add(loginField);

            JLabel passwordLabel = new JLabel("Password:");
            passwordField = new JPasswordField(20);

            panelFieldPassword.setLayout(new BoxLayout(panelFieldPassword, BoxLayout.Y_AXIS));
            panelFieldPassword.add(passwordLabel);
            panelFieldPassword.add(passwordField);

            JLabel emailLabel = new JLabel("Email");
            emailField = new JTextField(20);

            panelFieldEmail.setLayout(new BoxLayout(panelFieldEmail, BoxLayout.Y_AXIS));
            panelFieldEmail.add(emailLabel);
            panelFieldEmail.add(emailField);

            JButton submit = new JButton("Submit");
            submit.addActionListener(new SubmitButton());

            JButton cancel = new JButton("Cancel");
            cancel.addActionListener(new CancelButton());

            panelButtons.setLayout(new BoxLayout(panelButtons,BoxLayout.X_AXIS));
            panelButtons.add(submit);
            panelButtons.add(cancel);

            mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
            mainPanel.add(panelFieldLogin);
            mainPanel.add(panelFieldPassword);
            mainPanel.add(panelFieldEmail);
            mainPanel.add(panelButtons);

            signFrame.getContentPane().add(BorderLayout.NORTH, mainPanel);
            signFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            signFrame.setSize(270,250);
            signFrame.setVisible(true);
        }

        class SubmitButton implements ActionListener {

            public void actionPerformed(ActionEvent e) {
                writer.println(loginField.getText()
                        + ":" + passwordField.getText()
                        + ":" + emailField.getText());
                writer.flush();

                if (Client.recvMessage(reader).equals("Registration complete!")) {
                    new LogInGUI(reader,writer).createLogInGUI();
                    signFrame.setVisible(false);
                }
                else{
                    loginField.setText("");
                    passwordField.setText("");
                    emailField.setText("");
                    loginField.requestFocus();
                }
            }
        }

        class CancelButton implements ActionListener {

            public void actionPerformed(ActionEvent e) {

                new LogInGUI(reader,writer).createLogInGUI();
                signFrame.setVisible(false);
            }
        }

}
