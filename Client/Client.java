
import java.io.*;
import java.net.Socket;


public class Client {

    private BufferedReader reader;
    private PrintWriter writer;



    public static void main(String[] args) {

        Client client = new Client();
        client.setUpNetworking();

        LogInGUI logIn = new LogInGUI(client.reader,client.writer);
        logIn.createLogInGUI();
    }

    private void setUpNetworking(){

        try {
            Socket socket = new Socket("192.168.192.97", 10147);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }


    public static void sendMessage(PrintWriter writer, String message){
        writer.println(message);
        writer.flush();
    }

    public static String recvMessage(BufferedReader reader){

        String result = "exit";
        try {
            result = reader.readLine();
        } catch (IOException e) {
            System.out.println("Reader is failed!");

        }
        return result;
    }
}
