import java.net.Socket;

public class User {

    private String login;
    private Socket socket;

    public User(String login, Socket socket) {

        this.login = login;
        this.socket = socket;
        System.out.println("User " + this.login + " is created");
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Socket getSocket() {
        return socket;
    }
}
