

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {

    private final static String PASSWORD = "19120401";
    private final static String USERNAME = "tehnary9@gmail.com";

    private String receiver;
    private String subject;
    private String text;
    private String error;

    public Email setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public Email setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public Email setText(String text) {
        this.text = text;
        return this;
    }

    public String getError() {
        return error;
    }

    public boolean send() {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(USERNAME, PASSWORD);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.receiver));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(this.receiver));
            message.setSubject(this.subject);
            message.setText(this.text);

            Transport.send(message);

        } catch (MessagingException e) {
            error = e.getMessage();
            return false;
        }
        return true;
    }
}