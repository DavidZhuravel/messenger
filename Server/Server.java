import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

    private WorkWithMongoDB mongoDB;
    private ServerSocket serverSocket;
    private Socket clientSocket;


    private ArrayList<User> arrayActiveUsers = new ArrayList<User>();

    public static void main(String[] args) {

        Server server = new Server();
        server.start();
        server.waitForClient();

    }

    private void start(){

        mongoDB = new WorkWithMongoDB();
        mongoDB.clearChatTable();
        try {
            serverSocket = new ServerSocket(10147);
            System.out.println("Server is start!");
        } catch (IOException e) {
            System.out.println("Server isn`t created");
            System.exit(0);
        }
    }

    private void waitForClient(){

        while (true){

            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("We have connection: " + clientSocket.getInetAddress());

            Thread clientThread = new Thread(new ClientThread(clientSocket,mongoDB));
            clientThread.start();
        }
    }

    private User findBySocket(Socket socket){

        for (User activeUser : arrayActiveUsers) {
            if (activeUser.getSocket().equals(socket)) return activeUser;
        }
        return null;
    }

    private User findByLogin(String login){

        for (User activeUser : arrayActiveUsers) {
            if (activeUser.getLogin().equals(login)) return activeUser;
        }
        return null;
    }

    private void tellEveryone(String string){
        for (User activeUserInChat: arrayActiveUsers) {
            try {
                sendMessage(new PrintWriter(activeUserInChat.getSocket().getOutputStream()), string);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class ClientThread implements Runnable{

        Socket client;
        WorkWithMongoDB mongoDB;
        BufferedReader reader;
        PrintWriter writer;


        ClientThread(Socket client, WorkWithMongoDB mongoDB) {

            this.client = client;
            this.mongoDB = mongoDB;
            try {

                this.reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.writer = new PrintWriter(client.getOutputStream());
            }catch (IOException ex){
                System.out.println("Connection with " + client.getInetAddress() + "is failed");
            }
        }

        private void entranceToAccount(){

            while (true) {
                String message = recvMessage(reader);
                if ((message.split(":")).length == 2) {
                    if (logIn(message)) break;
                }
                else if ((message.split(":")).length == 3) {
                        registry(message);
                    }
                    else recoveryPassword(message);

            }
        }

        private boolean logIn(String message){

            if (client.isConnected()) {

                String login = message.split(":")[0];
                int password = Integer.valueOf(message.split(":")[1]);

                if (mongoDB.checkLogin(login)) {
                    if (!mongoDB.checkPassword(login, password))  return false;
                }else return false;
                sendMessage(writer, "Server: You can send message!");
                arrayActiveUsers.add(new User(login, client));
                return true;
            } return false;
        }

        private void registry(String message) {

            String login = message.split(":")[0];
            int password = Integer.valueOf(message.split(":")[1]);
            String email = message.split(":")[2];

            mongoDB.addToDBtableUsers(login,password,email);
            sendMessage(writer, "Registration complete!");


        }

        private void recoveryPassword(String message){

            if (mongoDB.checkEmail(message)){
                Email emailClient = new Email();

                emailClient
                        .setReceiver(message)
                        .setSubject("Recovery password in JavaChat")
                        .setText("In JavaChat your: \n"
                                + "-login: "  + mongoDB.getLoginOfUserByEmail(message) + "\n"
                                +"-password: " + mongoDB.getPasswordOfUserByEmail(message));

                if(!emailClient.send()) {
                    System.out.println(emailClient.getError());
                }
                sendMessage(writer,"Done");
            }else {
                sendMessage(writer,  "Wrong email!");
            }
        }

        private void sendToUser(){

            String message;
            try {
                message = sendAndTake("input login:message");
                sendMessage(new PrintWriter(findByLogin(message.split(":")[0]).getSocket().getOutputStream()),findBySocket(client).getLogin()+":"+message.split(":")[1]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void showActiveUsers(){

            sendMessage(writer,"List of active users:");
            for (User activeUser : arrayActiveUsers) {
                sendMessage(writer, "-" + activeUser.getLogin());
            }
        }

        public void run() {

            entranceToAccount();

            mongoDB.showChat(writer);

            while(client.isConnected()) {

                String message;

                message = recvMessage(reader);

                if (message.equals("exit")){
                    arrayActiveUsers.remove(findBySocket(client));
                    System.out.println("Client with IP:" + client.getInetAddress() + " is disconnect");
                    break;
                } else if (message.equals("show")) {
                    mongoDB.showChat(writer);

                } else if (message.equals("ShowActiveUsers")){
                    showActiveUsers();

                } else if (message.equals("/v")){
                    sendToUser();
                }
                else {
                    message = findBySocket(client).getLogin() + ": " + message;
                    tellEveryone(message);
                    mongoDB.addToDBtableChat(message);
                }
            }
            System.out.println("Connection have lost");
            try {
                arrayActiveUsers.remove(findBySocket(client));
                reader.close();
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private String sendAndTake(String message){

            sendMessage(writer, message);
            return recvMessage(reader);
        }


    }

    public static void sendMessage(PrintWriter writer, String message){
        writer.println(message);
        writer.flush();
    }

    public static String recvMessage(BufferedReader reader){

        String result = "exit";
        try {
            result = reader.readLine();
        } catch (IOException e) {
            System.out.println("Reader is failed!");

        }
        return result;
    }
}
