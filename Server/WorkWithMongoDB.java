import com.mongodb.*;

import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.Properties;

public class WorkWithMongoDB {

    private MongoClient client;
    private DB db;
    private boolean isConnect;
    private DBCollection tableChat;
    private DBCollection tableUsers;

    private Properties properties;

    public WorkWithMongoDB() {

        properties = new Properties();

        properties.setProperty("host", "localhost");
        properties.setProperty("port", "27017");
        properties.setProperty("DB_Name", "mydb");
        properties.setProperty("login", "root");
        properties.setProperty("password", "");
        properties.setProperty("tableChat", "Chat");
        properties.setProperty("tableUsers", "Users");

        try {

            //Connect to DataBase MongoDB
            client = new MongoClient(properties.getProperty("host"), Integer.valueOf(properties.getProperty("port")));

            //Choose DB for work
            db = client.getDB(properties.getProperty("DB_Name"));

            isConnect = db.authenticate(properties.getProperty("login"), properties.getProperty("password").toCharArray());

            //We enter the database under a given login and password
            tableChat = db.getCollection(properties.getProperty("tableChat"));

            tableUsers = db.getCollection(properties.getProperty("tableUsers"));


        } catch (UnknownHostException ex){
            System.err.println("Don`t connect to database");
        }
    }

    public void addToDBtableChat(String string){

            BasicDBObject dbObject = new BasicDBObject();

            dbObject.put("login", string.split(":")[0]);
            dbObject.put("message", string.split(":")[1]);
            tableChat.insert(dbObject);
    }

    public void addToDBtableUsers(String login, int password, String email){

        BasicDBObject newUser = new BasicDBObject();

        newUser.put("login", login);
        newUser.put("password", password);
        newUser.put("email", email);

        tableUsers.insert(newUser);
    }

    public void showChat(PrintWriter writer){

        DBCursor cursor = tableChat.find();
        for (DBObject obj: cursor) {
            String message = obj.get("login") + ":" + obj.get("message");
            Server.sendMessage(writer,message);
        }
    }

    public void showUsers(PrintWriter writer){

        DBCursor cursor = tableUsers.find();
        for (DBObject obj: cursor) {
            String message = String.valueOf(obj.get("login"));
            Server.sendMessage(writer,message);
        }
    }

    public String getPasswordOfUserByEmail(String email){

        BasicDBObject query = new BasicDBObject();
        query.put("email", email);

        DBObject result = tableUsers.findOne(query);

        return  String.valueOf(result.get("password"));
    }

    public String getLoginOfUserByEmail(String email){

        BasicDBObject query = new BasicDBObject();
        query.put("email", email);

        DBObject result = tableUsers.findOne(query);

        return  String.valueOf(result.get("login"));
    }

    public synchronized boolean checkLogin(String login){

        BasicDBObject query = new BasicDBObject();
        query.put("login", login);

        DBObject result = tableUsers.findOne(query);

        if (result!= null) return true;
        return false;
    }

    public synchronized boolean checkPassword(String login, int password){

        BasicDBObject query = new BasicDBObject();
        query.put("login", login);

        DBObject result = tableUsers.findOne(query);

        if (Integer.valueOf(String.valueOf(result.get("password"))) == password) return true;

        return false;
    }

    public synchronized boolean checkEmail(String email){

        BasicDBObject query = new BasicDBObject();
        query.put("email", email);

        DBObject result = tableUsers.findOne(query);

        if (result!= null) return true;
        return false;
    }

    public void clearChatTable(){
        tableChat.drop();
    }


}
